﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace VlcTvChecker
{
    public static class NetworkInterfaceFinder
    {
        public static IEnumerable<AddressToTryOut> FindInterfacesWithAddressInIPv4Range(IPAddress network, IPAddress mask)
        {
            var egligibleInterfaces = 
                NetworkInterface.GetAllNetworkInterfaces().Where(
                    ni => ni.Supports(NetworkInterfaceComponent.IPv4) && ni.SupportsMulticast &&
                          ni.OperationalStatus == OperationalStatus.Up);

            var addresses = new List<AddressToTryOut>();

            foreach (var netInterface in egligibleInterfaces)
            {
                var supportedIps = netInterface.GetIPProperties().UnicastAddresses.Where(
                    uip => uip.Address.AddressFamily == AddressFamily.InterNetwork &&
                           IPAddressUtils.IsAddressInNetwork(uip.Address, network, mask));
                addresses.AddRange(
                    supportedIps.Select(a =>
                    new AddressToTryOut { NetworkInterface = netInterface, UnicastAddressInformation = a }));
            }

            return addresses;
        }
    }
}
