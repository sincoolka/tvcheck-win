﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace VlcTvChecker
{
    class Program
    {
        static readonly IPAddress SAPMulticastAddress = IPAddress.Parse("224.2.127.254");
        static readonly int SAPMulticastPort = 9875;

        static int Main(string[] args)
        {
            bool scanWifi = false;
            bool scanEthernet = false;

            bool successEth = false;
            bool successWifi = false;

            bool interactive = true;

            int timeout = 10000;

            if (args.Contains("-h") || args.Contains("--help") || args.Contains("/?"))
            {
                Console.Error.WriteLine("Usage:\n {0} [--wifi] [--eth] [-a]", Environment.GetCommandLineArgs()[0]);
                Console.Error.WriteLine(" --wifi: tests wifi connections");
                Console.Error.WriteLine(" --eth: tests cable connections");
                Console.Error.WriteLine(" -a: non-interactive mode");
                Console.Error.WriteLine();
                Console.Error.WriteLine(
                    "DESCRIPTION:\n This command-line app attempts to test VIC CVUT TV through all network interfaces in the computer to see if it works. A join to the multicast group {0} on port {1} (SAP) is attempted and the received list of SAP channels is verified.", SAPMulticastAddress, SAPMulticastPort);
                Console.Error.WriteLine("REMARKS:\n A firewall might ask you to allow network access for this application. We recommend to allow both private and public networks access for the detection to work properly.");
                return 0;
            }

            
            if (args.Contains("--wifi"))
            {
                scanWifi = true;
            }
            if (args.Contains("--eth"))
            {
                scanEthernet = true;
            }
            if (args.Contains("-a"))
            {
                interactive = false;
            }
            if (!scanWifi && !scanEthernet)
            {
                scanWifi = true;
                scanEthernet = true;
            }


            Console.Error.WriteLine("Finding interfaces...");

            if (scanEthernet)
            {
                var range1 = NetworkInterfaceFinder.FindInterfacesWithAddressInIPv4Range(
                    IPAddress.Parse("147.32.110.0"), IPAddress.Parse("255.255.254.0"));
                var range2 = NetworkInterfaceFinder.FindInterfacesWithAddressInIPv4Range(
                    IPAddress.Parse("147.32.107.0"), IPAddress.Parse("255.255.255.0"));

                foreach (var netInterface in range1.Concat(range2))
                {
                    Console.Error.WriteLine("Trying '{0}' with IP {1}...", netInterface.NetworkInterface.Name, netInterface.UnicastAddressInformation.Address);
                    if (TryInterfaceMulticastSAP(netInterface, timeout))
                    {
                        successEth = true;
                        break;
                    }
                }
            }

            if (scanWifi)
            {
                var wifiRange = NetworkInterfaceFinder.FindInterfacesWithAddressInIPv4Range(
                    IPAddress.Parse("10.50.108.0"), IPAddress.Parse("255.255.252.0"));
                
                foreach (var netInterface in wifiRange)
                {
                    Console.Error.WriteLine("Trying Wifi interface '{0}' with IP {1}...",
                        netInterface.NetworkInterface.Name, netInterface.UnicastAddressInformation.Address);
                    if (TryInterfaceMulticastSAP(netInterface, timeout))
                    {
                        successWifi = true;
                        break;
                    }
                }
            }

            Console.Out.WriteLine();

            if (scanEthernet)
            {
                Console.Out.Write("Ethernet: ");
                if (successEth)
                    WriteLineInColor(ConsoleColor.Green, "PASS");
                else
                    WriteLineInColor(ConsoleColor.Red, "FAIL");
            }
            if (scanWifi)
            {
                Console.Out.Write("Wifi: ");
                if (successWifi)
                    WriteLineInColor(ConsoleColor.Green, "PASS");
                else
                    WriteLineInColor(ConsoleColor.Red, "FAIL");
            }

            Console.Error.WriteLine();

            if ((scanEthernet || scanWifi) && (!successEth && !successWifi))
            {
                Console.Error.WriteLine(
                    "NOTE:\n If a firewall asked you to allow network access for this application, please allow access to both private and public networks for the detection to work properly.");
            }
            else
            {
                Console.Error.WriteLine(
                    "NOTE:\n If VLC still does not work for you, check your firewall rules for VLC Media Player. You can also try disabling all other network interfaces in your computer.");
            }

            if (interactive)
            {
                Console.Error.WriteLine();
                Console.Error.WriteLine("Press any key to exit...");
                Console.ReadKey(true);
            }

            return (successEth || successWifi) ? 0 : 1;
        }

        static bool TryInterfaceMulticastSAP(AddressToTryOut netInterface, int timeout)
        {
            MulticastSocketConnector msc = new MulticastSocketConnector();

            try
            {
                msc.SetMulticastSocket(SAPMulticastAddress, SAPMulticastPort, netInterface);

                for (int i = 0; i < 100; i++)
                {
                    byte[] msg = msc.ReceiveOneMessageFromSocket(timeout);
                
                    if (msg == null) break;
                        
                    try
                    {
                        string receivedMsg = Encoding.UTF8.GetString(msg);
                        if (!string.IsNullOrWhiteSpace(receivedMsg) &&
                            receivedMsg.Contains("VIC CVUT"))
                        {
                            Console.Error.WriteLine("Looks OK, 'VIC CVUT' present!");
                            return true;
                        }

                        Console.Error.WriteLine("Receiving data...");
                    }
                    catch (Exception e)
                    {
                        Console.Error.WriteLine("Receiving data...");
                    }
                }
                
                Console.Error.WriteLine("Not this one, apparently...");
                return false;
            }
            finally
            {
                msc.CloseSocket();
            }
        }

        static void WriteInColor(ConsoleColor color, string format, params object[] arg)
        {
            var previousForeground = Console.ForegroundColor;
            Console.ForegroundColor = color;
            Console.Write(format, arg);
            Console.ForegroundColor = previousForeground;
        }
        static void WriteLineInColor(ConsoleColor color, string format, params object[] arg)
        {
            WriteInColor(color, format, arg);
            Console.WriteLine();
        }
    }
}
