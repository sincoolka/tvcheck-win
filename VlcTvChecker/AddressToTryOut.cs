﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;

namespace VlcTvChecker
{
    public class AddressToTryOut
    {
        public NetworkInterface NetworkInterface { get; set; }
        public UnicastIPAddressInformation UnicastAddressInformation { get; set; }
    }
}
