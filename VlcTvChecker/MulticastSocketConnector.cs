﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace VlcTvChecker
{
    public class MulticastSocketConnector
    {
        Socket udpSocket;
        bool multicastSocketSet = false;

        AddressToTryOut _mcastInterface;
        IPAddress _mcastAddress;
        int _mcastPort;

        public MulticastSocketConnector() { }

        public void SetMulticastSocket(IPAddress multicastAddress, int multicastPort, AddressToTryOut interfaceToBindTo)
        {
            if (udpSocket != null)
            {
                throw new InvalidOperationException($"Please create new {nameof(GetType)} to perform this operation.");
            }

            udpSocket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            udpSocket.ReceiveBufferSize = 16384;

            // HACK http://stackoverflow.com/questions/2192548/specifying-what-network-interface-an-udp-multicast-should-go-to-in-net
            // what is this piece of .... supposed to mean?
            IPv4InterfaceProperties p = interfaceToBindTo.NetworkInterface.GetIPProperties().GetIPv4Properties();
            if (p == null || p.Index < 0)
            {
                throw new ArgumentException("Entered interface cannot be used for multicast.", nameof(interfaceToBindTo));
            }
            udpSocket.SetSocketOption(SocketOptionLevel.IP, SocketOptionName.MulticastInterface, (int)IPAddress.HostToNetworkOrder(p.Index));
            
            udpSocket.Bind(new IPEndPoint(interfaceToBindTo.UnicastAddressInformation.Address, multicastPort));

            var multOpt = new MulticastOption(multicastAddress, interfaceToBindTo.UnicastAddressInformation.Address);
            udpSocket.SetSocketOption(SocketOptionLevel.IP, SocketOptionName.AddMembership, multOpt);

            multicastSocketSet = true;
            this._mcastInterface = interfaceToBindTo;
            this._mcastAddress = multicastAddress;
            this._mcastPort = multicastPort;
        }

        public byte[] ReceiveOneMessageFromSocket(int timeoutMs)
        {
            if (!multicastSocketSet)
            {
                throw new InvalidOperationException($"Call {nameof(SetMulticastSocket)} first.");
            }

            bool done = false;
            byte[] bytes = new byte[4096];
            IPEndPoint groupEP = new IPEndPoint(_mcastAddress, _mcastPort);
            EndPoint remoteEP = (EndPoint) new IPEndPoint(IPAddress.Any, 0);

            try
            {
                udpSocket.ReceiveTimeout = timeoutMs;
                int readbytes = udpSocket.ReceiveFrom(bytes, ref remoteEP);

                if (readbytes > 0)
                {
                    Array.Resize(ref bytes, readbytes);
                    return bytes;
                }
            }
            catch (SocketException e)
            {
                if (e.SocketErrorCode == SocketError.TimedOut)
                {
                    Console.Error.WriteLine("Receive: Timed out");
                }
                else
                {
                    Console.Error.WriteLine("Error: {0}", e.SocketErrorCode);
                    Console.Error.WriteLine(e.ToString());
                }
            }
            catch (Exception e)
            {
                Console.Error.WriteLine(e.ToString());
            }

            return null;
        }

        public void CloseSocket()
        {
            var multOpt = new MulticastOption(_mcastAddress, this._mcastInterface.UnicastAddressInformation.Address);
            udpSocket?.SetSocketOption(SocketOptionLevel.IP, SocketOptionName.DropMembership, multOpt);
            udpSocket?.Close();
        }
    }
}
