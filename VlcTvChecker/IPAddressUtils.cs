﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace VlcTvChecker
{
    public static class IPAddressUtils
    {
        public static bool IsAddressInNetwork(IPAddress address, IPAddress networkAddress, IPAddress mask)
        {
            if (address.AddressFamily != AddressFamily.InterNetwork &&
                networkAddress.AddressFamily != AddressFamily.InterNetwork &&
                address.AddressFamily != AddressFamily.InterNetworkV6 &&
                networkAddress.AddressFamily != AddressFamily.InterNetworkV6)
            {
                throw new ArgumentException("Cannot compare input address that is not IP or IPv6.");
            }

            if (address.AddressFamily != networkAddress.AddressFamily ||
                networkAddress.AddressFamily != mask.AddressFamily)
            {
                throw new ArgumentException("Cannot compare different types of addresses.");
            }

            byte[] addressBytes = address.GetAddressBytes();
            byte[] networkBytes = networkAddress.GetAddressBytes();
            byte[] maskBytes = mask.GetAddressBytes();

            // comparison
            for(int i = 0; i < addressBytes.Length; i++)
            {
                addressBytes[i] &= maskBytes[i];
                if (addressBytes[i] != networkBytes[i])
                {
                    return false;
                }
            }
            return true;
        }
    }
}
